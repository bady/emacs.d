;; -*- lexical-binding: t; -*-

;; Speedup startup time
;; Source: https://emacs.stackexchange.com/a/34367
(setq gc-cons-threshold-original gc-cons-threshold)
(setq gc-cons-threshold (* 1024 1024 100))
(setq file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

;; Reset the above settings after startup
(run-with-idle-timer 5 nil (lambda ()
   (setq gc-cons-threshold gc-cons-threshold-original)
   (setq file-name-handler-alist file-name-handler-alist-original)
   (makunbound 'gc-cons-threshold-original)
   (makunbound 'file-name-handler-alist-original)
   (message "gc-cons-threshold and file-name-handler-alist restored")))

;; Show startup time in message buffer
(add-to-list 'after-init-hook
             (lambda ()
               (message (concat "emacs (" (number-to-string (emacs-pid)) ") started in " (emacs-init-time)))))

;; Use custom font
(add-to-list 'default-frame-alist
             '(font . "Inconsolata Semi Condensed SemiBold 12"))
