;; -*- lexical-binding: t; -*-

;; A highly experimental emacs configuration file of a spacemacs refugee!

;; Enter debugger on errors
(setq-default debug-on-error t)


;; Add additional package repositories
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))


;; Install use-package if not already present
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;; Load use-package
(eval-when-compile
  (require 'use-package))


;; Install packages automatically if not aleady present
(require 'use-package-ensure)
(setq use-package-always-ensure t)


;; Auto-update packages every week
(use-package auto-package-update
  :custom
  ;; Ask for confirmation before updating
  (auto-package-update-prompt-before-update t)

  ;; Delete old versions of the upgraded packages
  (auto-package-update-delete-old-versions t)

  :config
  (auto-package-update-maybe))


;; Improved garbage collection
(use-package gcmh
  :config
  (gcmh-mode 1))


;; Store all backup and autosave files in the /tmp dir
;; Source: https://emacsredux.com/blog/2013/05/09/keep-backup-and-auto-save-files-out-of-the-way/
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))


;; Use separate file for storing customizations made using `M-x customize`
(setq custom-file (concat user-emacs-directory "custom.el"))
;; Create custom.el file if it doesn't exist
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file)


;; Load custom modules from init.d
;; Ref: https://medium.com/@zac.wood9/modularizing-init-el-5e9717d9795c
;; ---

;; Load path continaining custom configuration modules
(add-to-list 'load-path (concat user-emacs-directory "init.d"))

;; User interface enhacements
(require 'init-ui)

;; Core enhancements
(require 'init-core)

;; Custom functions
(require 'init-functions)

;; Completion frameworks
(require 'init-completion)

;; Window configuration management
(require 'init-workspaces)

;; Evil mode for vim-like keybindings and other evil features
(require 'init-evil)

;; Extra keybindings
(require 'init-keybindings)

;; Make working with coding projects easier
(require 'init-projects)

;; Developer modes for various languages
(require 'init-dev)

;; Web development modes
(require 'init-web)

;; Miscellaneous enhancements
(require 'init-misc)

;; Organize life in plain text
(require 'init-org)

;; Assist in using various modes, keymaps, etc.
(require 'init-help)
