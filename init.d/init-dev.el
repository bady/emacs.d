;; -*- lexical-binding: t; -*-

;; Python
(use-package elpy
  :init
  ;; Defer loading until python mode is loaded
  ;; Ref: https://elpy.readthedocs.io/en/latest/introduction.html#with-use-package
  (advice-add 'python-mode :before 'elpy-enable)

  :hook
  ;; Disable highlight-indentation-mode in favor of highlight-indent-guides-mode
  (elpy-mode . (lambda() (highlight-indentation-mode -1))))


;; Make this file available as a package
(provide 'init-dev)
