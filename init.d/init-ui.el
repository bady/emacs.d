;; -*- lexical-binding: t; -*-

;; Use doom monokai spectrum theme
(use-package doom-themes
  :config
  (load-theme 'doom-monokai-spectrum t)

  ;; Enable flashing mode-line on errorss
  (doom-themes-visual-bell-config)

  ;; Set custom faces
  (set-face-attribute 'default nil
        	      ;; :font "Inconsolata Semi Condensed SemiBold 12"
        	      :foreground "gray85"
        	      :background "#202026")

  (set-face-attribute 'region nil
                      :background "#606066")

  (set-face-attribute 'font-lock-string-face nil
        	      :foreground "khaki")

  (set-face-attribute 'font-lock-variable-name-face nil
        	      :foreground "PaleVioletRed"))


;; Group minor modes on mode-line
(use-package minions
  :config
  (minions-mode 1))


;; Doom mode-line
(use-package doom-modeline
  :custom
  ;; Don't show buffer encoding
  (doom-modeline-buffer-encoding nil)
  (doom-modeline-buffer-file-name-style 'truncate-with-project)

  :config
  (doom-modeline-mode 1)

  ;; Set mode-line faces
  (set-face-attribute 'mode-line nil
                      :background "#424648")
  (set-face-attribute 'mode-line-inactive nil
                      :background "#161820")

  ;; Set face of evil state indicators
  (set-face-attribute 'doom-modeline-evil-emacs-state nil
                      :foreground "deep sky blue")
  (set-face-attribute 'doom-modeline-evil-insert-state nil
                      :foreground "white")
  (set-face-attribute 'doom-modeline-evil-motion-state nil
                      :foreground "green")
  (set-face-attribute 'doom-modeline-evil-normal-state nil
                      :foreground "red")
  (set-face-attribute 'doom-modeline-evil-operator-state nil
                      :foreground "orange")
  (set-face-attribute 'doom-modeline-evil-visual-state nil
                      :foreground "magenta")
  (set-face-attribute 'doom-modeline-evil-replace-state nil
                      :foreground "brown")

  ;; Enable icons when using daemon mode
  ;; Source: http://sodaware.sdf.org/notes/emacs-daemon-doom-modeline-icons
  (add-hook 'after-make-frame-functions
            (lambda(_frame) (setq doom-modeline-icon t))))


;; Show dashboard on startup
(use-package dashboard
  :after
  ;; Projectile is required to show recent projects
  (projectile)

  :custom
  ;; Set custom banner image
  ;; Credits: Abraham Raji (https://commons.wikimedia.org/wiki/User:Avronr),
  ;; Kannan V M (https://commons.wikimedia.org/wiki/User:KannanVM)
  ;; Original source: https://commons.wikimedia.org/wiki/File:Mx-butterfly.svg
  (dashboard-startup-banner (expand-file-name
                             "emacs-evil-butterfly.png" user-emacs-directory))

  ;; Set custom banner title
  (dashboard-banner-logo-title "Real programmers use butterflies!")

  ;; Show 10 recent files and projects
  (dashboard-items '((projects . 10)
                     (recents . 10)))

  ;; Show number of packages loaded and init time
  (dashboard-set-init-info t)

  ;; Center align
  (dashboard-center-content t)

  ;; Show file icons
  (dashboard-set-file-icons t)

  :config
  (dashboard-setup-startup-hook)

  ;; Always show dashboard when emacsclient is started without file args
  (if (< (length command-line-args) 2)
      (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))))


;; Show line numbers
(use-package display-line-numbers
  :config
  ;; Show relative line numbers
  ;; (setq-default display-line-numbers-type 'relative)

  (global-display-line-numbers-mode 1))


;; Highlight indentation
(use-package highlight-indent-guides
  :hook
  (prog-mode . highlight-indent-guides-mode)

  :custom
  (highlight-indent-guides-method 'character)
  (highlight-indent-guides-character ?\u258F))


;; Highlight fill column (at column 80)
(use-package display-fill-column-indicator
  :custom
  (display-fill-column-indicator-column 80)

  :config
  (global-display-fill-column-indicator-mode t)
  (set-face-attribute 'fill-column-indicator nil :foreground "#282930"))


;; Show git diff on fringe (margin)
(use-package git-gutter-fringe
  :custom
  ;; Use right fringe
  (git-gutter-fr:side 'right-fringe)

  ;; Custom fringe elements as in spacemacs
  ;; Source: https://github.com/syl20bnr/spacemacs/blob/77d84b14e057aadc6a71c536104b57c617600f35/layers/%2Bsource-control/version-control/packages.el#L177
  (fringe-helper-define 'git-gutter-fr:added nil
    "..X...."
    "..X...."
    "XXXXX.."
    "..X...."
    "..X....")

  (fringe-helper-define 'git-gutter-fr:deleted nil
    "......."
    "......."
    "XXXXX.."
    "......."
    ".......")

  (fringe-helper-define 'git-gutter-fr:modified nil
    "..X...."
    ".XXX..."
    "XX.XX.."
    ".XXX..."
    "..X...."))


;; Dim inactive buffers
(use-package dimmer
  :custom
  ;; Make inactive buffer's text more dimmer (default is 0.2)
  (dimmer-fraction 0.4)

  :config
  ;; Exclude some buffers from dimming
  (dimmer-configure-which-key)
  (dimmer-configure-company-box)
  (dimmer-configure-magit)

  (dimmer-mode t))


;; UI-related one-liners from https://www.emacswiki.org/emacs/EmacsCrashCode
;; See 'init-core for other one-liners
;; ---

;; Remove toolbar
(tool-bar-mode -1)

;; Remove menubar
(menu-bar-mode -1)

;; Highlight current line
(global-hl-line-mode t)

;; Syntax highlighting
(global-font-lock-mode 1)

;; Show column number in mode-line
(column-number-mode t)

;; Highlight parenthesis pairs
(show-paren-mode 1)

;; Hihglight text between parenthesis
(setq show-paren-style 'expression)

;; Show empty lines
(setq-default indicate-empty-lines t)


;; Make this file available as a package
(provide 'init-ui)
