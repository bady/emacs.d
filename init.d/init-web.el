;; -*- lexical-binding: t; -*-

;; Emmet mode a.k.a zen mode
(use-package emmet-mode
  :hook
  ((web-mode css-mode) . emmet-mode))


;; PHP mode
(use-package php-mode)


;; A major mode that can work with many web template engines
(use-package web-mode
  :init
  ;; Highlight current HTML element
  (setq web-mode-enable-current-element-highlight t)

  ;; Highlight current column
  (setq web-mode-enable-current-column-highlight t)

  :mode
  ;; Enable web mode for HTML, CSS, JS & PHP files
  ("\\.html?\\'"
   "\\.css\\'"
   "\\.jsx?\\'"
   "\\.php\\'")

  :bind
  (:map web-mode-map
        ("C-n" . web-mode-tag-match)
        ("C-M-\\" . web-mode-buffer-indent))

  :custom
  (web-mode-enable-auto-pairing t)
  (web-mode-enable-css-colorization t)
  (web-mode-enable-block-face t)
  (web-mode-enable-part-face t)
  (web-mode-enable-comment-interpolation t)

  ;; Indentation settings
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-style-padding 2)
  (web-mode-script-padding 2)
  (web-mode-block-padding 2)
  (web-mode-comment-style 2)

  :config
  ;; Set custom face for blocks
  (set-face-attribute 'web-mode-block-face nil
                      :background "#151520")
  (set-face-attribute 'web-mode-current-element-highlight-face nil
                      :background "gray10")

  ;; Use web-mode with php-mode
  (add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))

  ;; Use django template engine by default for html files
  (setq web-mode-engines-alist
        '(("django" . "\\.html\\'")))

  :hook
  (web-mode . (lambda()
                (electric-pair-local-mode -1)
                (which-function-mode -1))))


;; Enhance web mode
(use-package web-mode-edit-element
  :after
  (web-mode)

  :hook
  (web-mode . web-mode-edit-element-minor-mode))


;; Instantly rename HTML tag pairs
(use-package auto-rename-tag
  :after
  (web-mode)

  ;; :custom
  ;; (auto-rename-tag-disabled-commands
  ;;  '(query-replace
  ;;    replace-string
  ;;    anzu-query-replace-regexp))

  :hook
  (web-mode . auto-rename-tag-mode))


;; Make this file available as a package
(provide 'init-web)
