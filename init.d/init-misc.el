;; -*- lexical-binding: t; -*-

;; Prevent [m]elpa package files from showing in recent files list
;; Ref: https://www.reddit.com/r/emacs/comments/5wydsd/stop_recent_files_showing_elpa_packages/
(use-package recentf
  :config
  (recentf-mode)
  (setq  recentf-max-saved-items 1000
         recentf-exclude '("^/var/folders\\.*"
                           "COMMIT_EDITMSG\\'"
                           ".*-autoloads\\.el\\'"
                           "[/\\]\\.elpa/")))


;; Better code highlighting and more
(use-package tree-sitter
  :if (executable-find "tree-sitter")
  :hook
  ((python-mode
    php-mode
    html-mode
    css-mode
    js-mode) . tree-sitter-mode)
  (tree-sitter-after-on . tree-sitter-hl-mode))


;; Language bundle for tree-sitter
(use-package tree-sitter-langs
  :after
  (tree-sitter))


;; Smooth scrolling
(use-package smooth-scrolling
  :config
  (smooth-scrolling-mode 1))


;; Format buffers
(use-package format-all
  :bind
  ("C-M-\\" . format-all-buffer))


;; Use fasd for quick navigation
(use-package fasd
  :config
  (global-fasd-mode 1))


;; Code folding
(use-package hideshow
  :bind
  ("C-<iso-lefttab>" . hs-hide-all)
  ("C-M-<iso-lefttab>" . hs-show-all)
  ("C-M-<tab>" . hs-toggle-hiding)

  :hook
  (prog-mode . hs-minor-mode))


;; Preview line when executing goto-line command
(use-package goto-line-preview
  :bind
  ([remap goto-line] . goto-line-preview))


;; Ranger-like dired file manager
(use-package ranger
  :custom
  ;; Show hidden files and directories by default
  (ranger-show-hidden nil)

  ;; Don't hide cursor
  (ranger-hide-cursor nil)

  :config
  (ranger-override-dired-mode t))


;; Learn to reduce hand movements
(use-package guru-mode
  :after
  (evil)

  :config
  (guru-global-mode +1)
  (evil-global-set-key 'motion (kbd "<left>") nil)
  (evil-global-set-key 'motion (kbd "<down>") nil)
  (evil-global-set-key 'motion (kbd "<up>") nil)
  (evil-global-set-key 'motion (kbd "<right>") nil))


;; Add option to restart emacs within emacs
(use-package restart-emacs)


;; Make this file available as a pacakge
(provide 'init-misc)
