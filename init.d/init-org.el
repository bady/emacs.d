;; -*- lexical-binding: t; -*-

;; Org mode settings
(use-package org
  :bind
  (("C-c a" . org-agenda)
   ("C-c c" . org-roam-capture)
   ("C-c l" . org-store-link)
   ("C-c w" . org-refile-to-other-file)
   ("M--" . org-toggle-heading))

  (:map org-mode-map
        ;; Move subtrees
        ("M-J" . org-move-subtree-down)
        ("M-K" . org-move-subtree-up)

        ;; Change heading levels
        ("M-H" . org-promote-subtree)
        ("M-L" . org-demote-subtree))

  :custom
  ;; Enable word wrapping
  (org-startup-truncated nil)

  ;; Hide leading stars
  (org-hide-leading-stars t)

  ;; Use custom ellipsis
  (org-ellipsis "⤵")

  ;; Don't insert blank lines before new entries
  (org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))

  ;; Set TODO keywords
  (org-todo-keywords
   '((sequence "TODO(t!)" "NEXT(n)" "WIP(w!)" "|" "DONE(d!)")
     (sequence "HOLD(h!)" "DELEGATED(o!)" "|" "SOMEDAY(s!)" "CANCELLED(c!)")))

  ;; Set tags
  (org-tag-alist '(("@alphafork" . ?a) ("@pirates" . ?i)
                   (:startgrouptag) ("@personal" . ?p)
                   (:grouptags)
                   ("@home" . ?h) ("@call" . ?c) ("@read" . ?r) ("@watch" . ?w)
                   (:endgrouptag)
                   ("@fsci" . ?f) ("@fsfi" . ?s)))

  ;; Log time of task completion into drawer
  (org-log-into-drawer t)

  ;; Save clocking history across emacs sessions
  (org-clock-persist 'history)

  :config
  ;; Set up hooks for clock persistence
  (org-clock-persistence-insinuate)

  ;; Encryption settings
  (org-crypt-use-before-save-magic)
  (setq org-crypt-key "0xD97F657A735428A8BF5503E158A3B4EE953C117D")

  ;; Fix org mode overriding custom window nav keybindings in evil normal state
  (define-key org-mode-map (kbd "<normal-state> M-h") nil)
  (define-key org-mode-map (kbd "<normal-state> M-j") nil)
  (define-key org-mode-map (kbd "<normal-state> M-k") nil)
  (define-key org-mode-map (kbd "<normal-state> M-l") nil)

  :hook
  (org-mode . flyspell-mode))


;; Org journal for diary
(use-package org-journal
  :init
  (setq org-journal-prefix-key "C-c j")

  :custom
  (org-journal-dir "~/Documents/cloud/journal")
  (org-journal-enable-encryption t)
  (org-journal-enable-cache t))


;; Org roam for zettelkasten note-taking
(use-package org-roam
  :init
  ;; Set org-roam directory
  (setq org-roam-directory "~/Documents/cloud/second-brain")

  ;; Acknowledge v2 migration
  (setq org-roam-v2-ack t)

  :bind
  ("C-c b" . org-roam)
  ("C-c i" . org-roam-insert)

  :config
  ;; Enable org-roam-protocol
  (require 'org-roam-protocol)

  :hook
  ;; Load org-roam-mode on startup
  (after-init . org-roam-mode))


;; Beautify bullets
(use-package org-superstar
  :after
  (org)

  :custom
  ;; Use custom bullet symbols
  (org-superstar-headline-bullets-list '("◉" "✸" "➤" "♦" "➔"))

  ;; Hide leading dots
  (org-superstar-leading-bullet nil)

  :hook
  ;; Enable org-superstar-mode
  (org-mode . org-superstar-mode))


;; Quickly browse and filter org files
(use-package deft
  :after
  (org)

  :custom
  (deft-recursive t)
  (deft-default-extension "org")
  (deft-directory org-roam-directory)
  (deft-auto-save-interval 0)
  (deft-use-filename-as-title t)

  ;; Strip newlines, properties, keywords and links from deft summary
  (deft-strip-summary-regexp "[\n\t]\\|^:PROPERTIES:\n\\(.+\n\\)+:END:\n\\|^#\\+[[:lower:][:upper:]_?]+:.*$\\|\\[.+\\]")

  :hook
  (deft-mode . (lambda() (display-line-numbers-mode -1))))


;; Insert org-mode links from clipboard
(use-package org-cliplink)


;; Rich text clipboard for org-mode
;; Paste into #+BEGIN_SRC block of correct mode, with link to where it came from.
(use-package org-rich-yank
  :bind
  (:map org-mode-map
        ("C-M-y" . org-rich-yank)))


;; Yank images to org files from clipboard
(use-package org-download
  :after
  (org)

  :bind
  (:map org-mode-map
        ("s-y" . org-download-clipboard)))


;; Annotate pdf and other docs
(use-package org-noter
  :bind
  ("C-c n" . org-noter))


;; Manage citation from zotero
(use-package zotxt
  :after
  (org)

  :bind
  (:map org-mode-map
        ("C-c z i" . org-zotxt-insert-reference-link)
        ("C-c z u" . org-zotxt-update-reference-link-at-point)
        ("C-c z o" . org-zotxt-open-attachment)
        ("C-c z n" . org-zotxt-noter))

  :hook
  (org-mode . org-zotxt-mode))


;; Make this file available as a package
(provide 'init-org)
