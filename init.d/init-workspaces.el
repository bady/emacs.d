;; -*- lexical-binding: t; -*-

;; Named window configurations that can be saved to file
(use-package persp-mode
  :demand t

  :bind-keymap
  ("M-o" . persp-key-map)

  :bind
  (("C-<tab>" . persp-switch)
   ("C-x k" . persp-kill-buffer))

  :custom
  ;; Set default perspective name
  (persp-nil-name "default")

  ;; Autosave perspectives
  (persp-auto-save-opt 1)

  ;; Don't autoload perspectives
  (persp-auto-resume-time 0)

  ;; Kill persp buffers from anywhere without asking for confirmation
  (persp-kill-foreign-buffer-behaviour 'kill)

  :config
  (persp-mode)

  ;; Integrate ivy with persp-mode
  ;; Source: https://gist.github.com/Bad-ptr/1aca1ec54c3bdb2ee80996eb2b68ad2d#file-persp-ivy-el
  (with-eval-after-load "ivy"
    (add-hook 'ivy-ignore-buffers
              #'(lambda (b)
                  (when persp-mode
                    (let ((persp (get-current-persp)))
                      (if persp
                          (not (persp-contain-buffer-p b persp))
                        nil)))))

    (setq ivy-sort-functions-alist
          (append ivy-sort-functions-alist
                  '((persp-kill-buffer   . nil)
                    (persp-remove-buffer . nil)
                    (persp-add-buffer    . nil)
                    (persp-switch        . nil)
                    (persp-window-switch . nil)
                    (persp-frame-switch  . nil))))))


;; Automatically create a perspective for each projectile project
(use-package persp-mode-projectile-bridge
  :after
  (projectile)

  :config
  (add-hook 'persp-mode-projectile-bridge-mode-hook
            #'(lambda ()
                (if persp-mode-projectile-bridge-mode
                    (persp-mode-projectile-bridge-find-perspectives-for-all-buffers)
                  (persp-mode-projectile-bridge-kill-perspectives))))
  (add-hook 'after-init-hook
            #'(lambda ()
                (persp-mode-projectile-bridge-mode 1))
            t))


;; Resize active window for more space
(use-package zoom
  :custom
  ;; Set zoom size as in golden-ratio-mode
  (zoom-size '(0.618 . 0.618))

  ;; Ignore some major modes and buffers
  (zoom-ignored-major-modes '(dired-mode ranger-mode help-mode helpful-mode help-mode-menu org-mode))
  (zoom-ignored-buffer-name-regexps '("\\*.*\\*"))

  :config
  (zoom-mode t))


;; Swap windows
(use-package windswap
  :config
  (windswap-default-keybindings 'control))


;; Rearrange window configuration
(use-package transpose-frame)


;; Make this file available as a package
(provide 'init-workspaces)
