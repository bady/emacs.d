;; -*- lexical-binding: t; -*-

;; Show line of matching block in minibuffer
;; Source: https://stackoverflow.com/a/29949317
(defvar match-paren--idle-timer nil)
(defvar match-paren--delay 0.2)
(setq match-paren--idle-timer
      (run-with-idle-timer match-paren--delay t #'blink-matching-open))


;; Display current function name in mode-line
(use-package which-func
  :custom
  ;; Custom which-func-format with no square brackets around the function name
  (which-func-format
   '(:propertize which-func-current local-map
                 (keymap
                  (mode-line keymap
                             (mouse-3 . end-of-defun)
                             (mouse-1 . beginning-of-defun)))))

  :config
  (which-function-mode 1)

  ;; Show empty line when function name cannot be determined
  (setq which-func-unknown ""))


;; On keypress show available keybindings automatically on popup
(use-package which-key
  :custom
  ;; Group all prefix keys at the end
  (which-key-sort-order 'which-key-prefix-then-key-order)

  :config
  (which-key-mode))


;; Enhanced help mode
(use-package helpful
  :config
  (setq counsel-describe-function-function #'helpful-callable)
  (setq counsel-describe-variable-function #'helpful-variable))


;; Discover keybindings of the current major mode
(use-package discover-my-major
  :bind
  ("C-h C-m" . discover-my-major))


;; Show free keybindings in current buffer
(use-package free-keys)


;; Highlight known Emacs Lisp symbols
(use-package highlight-defined
  :hook
  (emacs-lisp-mode . highlight-defined-mode))


;; Track frequency of commands
;; Use command `keyfreq-show` to display statistics
(use-package keyfreq
  :config
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1))


;; Make this file available as a pacakge
(provide 'init-help)
