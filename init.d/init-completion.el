;; -*- lexical-binding: t; -*-

;; Ivy settings
(use-package ivy
  :init
  (use-package counsel :config (counsel-mode))
  (use-package swiper :defer t)
  (use-package ivy-hydra :defer t)
  (use-package ivy-avy)

  :bind
  (("C-c C-s" . swiper)
   ("C-c s" . swiper-thing-at-point)
   ("C-c C-r" . ivy-resume)
   ("M-n" . counsel-M-x)
   ("C-x C-f" . counsel-find-file)
   ("<f1> f" . counsel-describe-function)
   ("<f1> v" . counsel-describe-variable)
   ("<f1> o" . counsel-describe-symbol)
   ("<f1> l" . counsel-find-library)
   ("<f2> i" . counsel-info-lookup-symbol)
   ("<f2> u" . counsel-unicode-char))

  :custom
  ;; Show both index and count of matched items
  (ivy-count-format "%d/%d ")

  ;; Show bookmarks and recent files on disk in buffers list
  (ivy-use-virtual-buffers t)

  ;; Enable fuzzy matching
  (ivy-re-builders-alist '((t . ivy--regex-fuzzy)))

  ;; Avoid adding caret (^) to the regex
  (ivy-initial-inputs-alist nil)

  ;; Remove current & parent directories from counsel-find-file
  (ivy-extra-directories nil)

  ;; Stop ivy from quitting when backspace is carelessly pressed many times
  (ivy-on-del-error-function #'ignore)

  ;; Use ivy-hydra for ivy-dispatching-done (M-o)
  ;; Ref: https://github.com/abo-abo/swiper/issues/2397
  (ivy-read-action-function 'ivy-hydra-read-action)

  :custom-face
  (ivy-current-match ((t (:weight bold :background "#0b0d0f"))))
  (ivy-minibuffer-match-face-1 ((t (:foreground "gray50" :background "#363840"))))
  (ivy-minibuffer-match-face-2 ((t (:foreground "orange red"))))
  (ivy-minibuffer-match-face-3 ((t (:foreground "yellow2"))))
  (ivy-minibuffer-match-face-4 ((t (:foreground "khaki"))))
  (ivy-subdir ((t (:foreground "gray50"))))
  (ivy-virtual ((t (:foreground "gray85"))))

  :config
  (ivy-mode 1)

  ;; Swap keybindings for ivy-done and ivy-alt-done
  ;; Use enter key to expand directories instead of opening in dired
  ;; Source: https://emacs.stackexchange.com/a/33706
  (with-eval-after-load 'counsel
    (let ((done (where-is-internal #'ivy-done ivy-minibuffer-map t))
          (alt (where-is-internal #'ivy-alt-done ivy-minibuffer-map t)))
      (define-key counsel-find-file-map done #'ivy-alt-done)
      (define-key counsel-find-file-map alt  #'ivy-done))))


;; Smart sorting and filtering based on history and frequency
(use-package prescient
  :custom
  (prescient-sort-length-enable nil)
  ;; Search for literal match first and then go fuzzy
  (prescient-sort-full-matches-first t)
  (prescient-filter-method '(fuzzy regexp))

  :config
  ;; Save command history on disk, so the sorting gets more intelligent over time
  (prescient-persist-mode +1))


(use-package ivy-prescient
  :after
  (ivy)

  :custom
  ;; Emulate default ivy highlighting
  (ivy-prescient-retain-classic-highlighting t)

  :config
  ;; Make company use prescient for smart sorting and filtering
  (ivy-prescient-mode 1))


;; Make ivy mode more information rich
(use-package ivy-rich
  :config
  (ivy-rich-mode 1)
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))


;; Company (complete anything) mode settings
(use-package company
  :bind
  (:map company-active-map
        ("M-j" . 'company-select-next)
        ("M-k" . 'company-select-previous))

  :custom
  ;; Start showing suggestions as soon as typing 2 characters
  (company-minimum-prefix-length 2)

  ;; Reduce delay to 0.2s
  (company-idle-delay 0.2)

  ;; Enable case-insenstive completion
  (company-dabbrev-ignore-case t)

  ;; Don't change case of returned candidates
  (company-dabbrev-downcase nil)

  ;; Cycle between suggestions
  (company-selection-wrap-around t)

  ;; Align annotations to the right tooltip border
  (company-tooltip-align-annotations t)

  ;; Use numbers for quick completion
  (company-show-numbers t)

  ;; Allow to continue by typing non-matching characters
  (company-require-match 0)

  ;; Custom backends including file completion support
  ;; Ref: https://www.reddit.com/r/emacs/comments/c63mzr/a_correct_company_backends_list/
  (company-backends '(company-files company-keywords company-capf company-dabbrev))

  :config
  ;; Enable company-mode globally
  (global-company-mode 1)

  ;; Use tab key to cycle through suggestions ('tng' -> 'tab and go')
  (company-tng-mode 1))


;; Enable fuzzy completion
(use-package company-flx
  :after
  (company)

  :config
  (company-flx-mode +1))


;; Make company completions more information rich
(use-package company-box
  :after
  (company)

  :hook
  (company-mode . company-box-mode))


;; Snippets based competion
(use-package yasnippet
  ;; Pre-defined collection of snippets
  :ensure yasnippet-snippets

  :config
  (yas-global-mode 1))


;; Word correction using avy
(use-package flyspell-correct-avy-menu
  :init
  (use-package flyspell-correct :after flyspell)

  :after
  (flyspell-correct))


;; Make this file available as a package
(provide 'init-completion)
