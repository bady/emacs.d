;; -*- lexical-binding: t; -*-

;; Easy project management and navigation
(use-package projectile
  :bind-keymap
  ("M-p" . projectile-command-map)

  :config
  (projectile-mode +1))


;; Improved projectile integration with counsel
(use-package counsel-projectile
  :after
  (counsel)

  :custom
  ;; Sort files by frecency
  (counsel-projectile-sort-files t)

  :config
  (counsel-projectile-mode))


;; Git at the speed of thought
(use-package magit
  :hook
  (magit-mode . toggle-truncate-lines))


;; Jump to definition
(use-package dumb-jump
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))


;; Eglot LSP (Language Server Protocol) client
(use-package eglot
  :bind
  (:map eglot-mode-map
        ("<f12>" . xref-find-definitions)
        ("C-M-\\" . eglot-format-buffer))

  :hook
  (python-mode . eglot-ensure))


;; Make this file available as a package
(provide 'init-projects)
