;; -*- lexical-binding: t; -*-

;; Source: https://www.emacswiki.org/emacs/MiniBuffer
(defun switch-to-minibuffer ()
  "Switch to minibuffer"
  (interactive)
  (if (active-minibuffer-window)
      (select-window (active-minibuffer-window))
    (error "Minibuffer is not active")))


;; Source: https://gist.github.com/3402786
(defun toggle-maximize-buffer ()
  "Toggle maximize current buffer"
  (interactive)
  (save-excursion
    (if (and (= 1 (length (window-list)))
             (assoc ?_ register-alist))
        (jump-to-register ?_)
      (progn
        (window-configuration-to-register ?_)
        (delete-other-windows)))))


;; Source: https://stackoverflow.com/a/7250027
(defun smart-line-beginning ()
  "Move point to the beginning of text on the current line; if that is already
  the current position of point, then move it to the beginning of the line."
  (interactive)
  (let ((pt (point)))
    (beginning-of-line-text)
    (when (eq pt (point))
      (beginning-of-line))))


;; Source: https://emacs.stackexchange.com/a/12651
(defun projectile-find-file-maybe ()
  "When inside a project, show only project-related files"
  (interactive)
  (call-interactively
   (if (projectile-project-p)
       #'counsel-projectile-find-file
     #'counsel-find-file)))


(defun projectile-switch-to-buffer-maybe ()
  "When inside a project, show only project-related buffers"
  (interactive)
  (call-interactively
   (if (projectile-project-p)
       #'counsel-projectile-switch-to-buffer
     #'switch-to-buffer)))


(defun projectile-previous-project-buffer-maybe ()
  "When inside a project, switch to previous project buffer"
  (interactive)
  (call-interactively
   (if (projectile-project-p)
       #'projectile-previous-project-buffer
     #'previous-buffer)))


(defun projectile-next-project-buffer-maybe ()
  "When inside a project, switch to next project buffer"
  (interactive)
  (call-interactively
   (if (projectile-project-p)
       #'projectile-next-project-buffer
     #'next-buffer)))


(defun open-file-window-right ()
  "Split window vertically and open file"
  (interactive)
  (split-window-right)
  (windmove-right)
  (projectile-find-file-maybe))


(defun open-file-window-below ()
  "Split window horizontally and open file"
  (interactive)
  (split-window-below)
  (windmove-down)
  (projectile-find-file-maybe))


;; Source: https://emacs.stackexchange.com/a/63148
(defun toggle-line-numbering ()
  "Toggle line numbering between absolute and relative"
  (interactive)
  (if (eq display-line-numbers 'relative)
      (setq display-line-numbers t)
    (setq display-line-numbers 'relative)))


;; Source: http://rejeep.github.io/emacs/elisp/2010/03/26/rename-file-and-buffer-in-emacs.html
(defun rename-this-buffer-and-file ()
  "Renames current buffer and file it is visiting"
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (cond ((get-buffer new-name)
               (error "A buffer named '%s' already exists!" new-name))
              (t
               (rename-file filename new-name 1)
               (rename-buffer new-name)
               (set-visited-file-name new-name)
               (set-buffer-modified-p nil)
               (message "File '%s' successfully renamed to '%s'" name (file-name-nondirectory new-name))))))))


(defun mark-line-at-point ()
  "Mark current line"
  (interactive)
  (set-mark (line-beginning-position))
  (end-of-line))


(defun eval-buffer-verbose ()
  "Evaluate buffer with feedback"
  (interactive)
  (eval-buffer)
  (message "Buffer evaluated"))


(defun open-emacs-directory ()
  "Open emacs configuration directory"
  (interactive)
  (ranger-find-file (concat user-emacs-directory "init.d")))


(defun open-org-roam-directory ()
  "Open org-roam directory"
  (interactive)
  (ranger-find-file org-roam-directory))


(defun org-refile-to-other-file ()
  "Move org subtree to a new file"
  (interactive)
  (org-copy-subtree nil t)
  (org-roam-find-file)
  (goto-char (point-max))
  (org-paste-subtree))


;; Make this file available as a package
(provide 'init-functions)
