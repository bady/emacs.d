;; -*- lexical-binding: t; -*-

;; Various useful one-liners
;; ---

;; Skip virtual buffers when switching recent buffers
;; Source: https://emacs.stackexchange.com/q/27749
(set-frame-parameter (selected-frame) 'buffer-predicate #'buffer-file-name)

;; Replace active region on insert
(delete-selection-mode 1)

;; Auto-insert matching delimiter
(electric-pair-mode 1)

;; Keep buffers automatically up-to-date
(global-auto-revert-mode 1)

;; Save minibuffer history for future sessions
(savehist-mode 1)

;; Save recent files for future sessions
(recentf-mode 1)

;; Don't spawn new buffers on entering each directory
(put 'dired-find-alternate-file 'disabled nil)

;; Autofocus help windows
(setq help-window-select t)

;; Set major mode for the initial scratch buffer
(setq initial-major-mode 'fundamental-mode)

;; Don't auto-resize frames
(setq frame-inhibit-implied-resize t)


;; One-liners from https://www.emacswiki.org/emacs/EmacsCrashCode
;; See 'init-ui for UI-related one-liners
;; ---

;; Use buffer name as frame title
(setq frame-title-format "%b - Emacs")

;; Make file-completion case-insenstitve
(setq read-file-name-completion-ignore-case 't)

;; Make buffer completion case-insensitive
(setq read-buffer-completion-ignore-case 't)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Set a tab character to 2 spaces
(setq tab-width 2)

;; Use y/n instead of yes/no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Remove trailing whitespaces on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)


;; Improve core editor functionalities using external packages
;; ---

;; Undo tree
(use-package undo-tree
  :config
  (global-undo-tree-mode))


;; Interactively insert from kill-ring
(use-package popup-kill-ring
  :bind
  ("M-y" . popup-kill-ring))


;; Improved search and replace
(use-package anzu
  :bind
  (("C-S-s" . anzu-query-replace)
   ("C-S-r" . anzu-query-replace-regexp))

  :custom
  ;; Avoid duplicate mode-line highlight when using spaceline or doom modeline
  (anzu-cons-mode-line-p nil)

  :config
  (global-anzu-mode +1))


;; Jump to visible text or line using a char-based decision tree
(use-package avy
  :demand t)


;; Smart and integrated search and navigation
(use-package ace-isearch
  :init
  (global-ace-isearch-mode +1)

  :bind
  (:map isearch-mode-map
  ("C-f" . ace-isearch-jump-during-isearch))

  :custom
  (ace-isearch-jump-based-on-one-char nil)
  (ace-isearch-2-function 'avy-goto-char-2)
  (ace-isearch-on-evil-mode t))


;; Delete multiple whitespaces at once
(use-package smart-hungry-delete
  :bind
  (("S-<backspace>" . smart-hungry-delete-backward-char)
   ("C-d" . smart-hungry-delete-forward-char))

  :config (smart-hungry-delete-add-default-hooks))


;; Improved zap-to-char to delete text between the point and specified character
(use-package zzz-to-char
  :bind
  ("M-z" . zzz-to-char))


;; Wrap region with punctuations or tags
(use-package wrap-region
  :config
  (wrap-region-mode t))


;; Drag words, lines and region
(use-package drag-stuff
  :config
  (drag-stuff-global-mode 1)
  (drag-stuff-define-keys))


;; Expand selected region by semantic units
(use-package expand-region
  :bind
  ("C-=" . er/expand-region))


;; Improved kill and mark
(use-package easy-kill
  :bind
  (([remap kill-ring-save] . easy-kill)
   ([remap mark-sexp] . easy-mark)))


;; Multiple cursors
(use-package multiple-cursors
  :bind
  (("C-S-<mouse-1>" . mc/add-cursor-on-click)))


;; Enable custom bindings for active region
(use-package region-bindings-mode
  :bind
  ;; Bindings for extending multiple cursors in active region
  (:map region-bindings-mode-map
        ("C-i" . 'mc/edit-lines)
        ("C-o" . 'mc/mark-all-like-this)
        ("C-p" . 'mc/mark-previous-like-this)
        ("C-n" . 'mc/mark-next-like-this)
        ("C-m" . 'mc/mark-more-like-this-extended)
        ("C-k" . 'kill-whole-line)
        ("C-l" . 'mark-line-at-point))

  :config
  (region-bindings-mode-enable))


;; Make this file available as a package
(provide 'init-core)
